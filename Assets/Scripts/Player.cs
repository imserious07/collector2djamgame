﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    private const float GROW_SIZE = 0.22f;
    private const float MOVE_DIST = 0.5f;

    private direction lastWrongDirection;

    private direction _currentDirection = direction.NONE;
    public direction currentDirection {
        set {
            _currentDirection = value;

            if (_currentDirection != direction.NONE) {
                startPoint = transform.position;
                isMoving = true;
                moveValue = 0;
                startMovingTime = Time.time;

                switch (_currentDirection) {
                    case direction.LEFT:
                        endPoint = transform.position + (Vector3.left * MOVE_DIST);
                        break;
                    case direction.RIGHT:
                        endPoint = transform.position + (Vector3.right * MOVE_DIST);
                        break;
                    case direction.UP:
                        endPoint = transform.position + (Vector3.up * MOVE_DIST);
                        break;
                    case direction.DOWN:
                        endPoint = transform.position + (Vector3.down * MOVE_DIST);
                        break;
                }

                //float posX = endPoint.x;
                //float posY = endPoint.y;

                //if (posX % MOVE_DIST != 0)
                //    posX = endPoint.x - posX % MOVE_DIST;

                //if (startPoint.y % MOVE_DIST != 0)
                //    posY = endPoint.y - posY % MOVE_DIST;

                //endPoint = new Vector3(posX, posY, endPoint.z);

                journeyLength = Vector3.Distance(startPoint, endPoint);
            }
        }
        get {
            return _currentDirection;
        }
    }

    // Position
    private Vector3 startPoint;
    private Vector3 endPoint;

    // Size
    private Vector3 startSize;
    private Vector3 finishSize;

    private float _sizeValue = 1;
    private float sizeValue {
        set {
            _sizeValue = value;

            if (_sizeValue >= 1) {
                transform.localScale = finishSize;
            }
        }

        get {
            return _sizeValue;
        }
    }

    private float _moveValue = 0;
    private float moveValue {
        set {
            _moveValue = value;

            if (_moveValue >= 1) {
                transform.position = endPoint;

                if (movingBack) {
                    float posX = transform.position.x;
                    float posY = transform.position.y;

                    Vector3 newPosition = transform.position;

                    if (transform.position.x % MOVE_DIST != 0) {
                        posX = transform.position.x - (transform.position.x % MOVE_DIST);
                    }

                    if (transform.position.y % MOVE_DIST != 0) {
                        posY = transform.position.y - (transform.position.y % MOVE_DIST);
                    }

                    newPosition = new Vector3(posX, posY, newPosition.z);

                    transform.position = newPosition;
                }

                currentDirection = direction.NONE;

                movingBack = false;
                isMoving = false;
                
            }
        }

        get {
            return _moveValue;
        }
    }

    float startGrowingTime;

    float distCovered;
    float startMovingTime;
    public float moveSpeed = 1f;
    public float growSpeed = 1f;
    float journeyLength;

    // Side points
    public Transform topOverlapPoint;
    public Transform bottomOverlapPoint;
    public Transform leftOverlapPoint;
    public Transform rightOverlapPoint;

    // Corner points

    public Transform topLeftCorner;
    public Transform topRightCorner;
    public Transform bottomLeftCorner;
    public Transform bottomRightCorner;

    public LayerMask wallsMask;

    private bool movingBack = false;
    private bool isMoving = false;
    [HideInInspector]
    public bool canMove = true;
    private float changeColorCounter = 0;

    private BlackOnStart blackOnStart;
    private DontDestroyMusic music;
    private SpeedUpIcon speedUpIcon;

    private AudioSource audioSource;
    private SpriteRenderer spriteRenderer;

    public bool canSpeedUpTime = false;

    [Header("Audio")]
    public AudioClip pickupCoin;
    public AudioClip growUp;
    public AudioClip door;

    private void Awake() {
        blackOnStart = FindObjectOfType<BlackOnStart>();
        audioSource = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        music = FindObjectOfType<DontDestroyMusic>();
        speedUpIcon = FindObjectOfType<SpeedUpIcon>();
    }

    private void UndoMovement() {
        if (!movingBack && isMoving) {
            lastWrongDirection = currentDirection;

            movingBack = true;
            endPoint = startPoint;
            startPoint = transform.position;
            moveValue = 0;
            startMovingTime = Time.time;
            journeyLength = Vector3.Distance(startPoint, endPoint);

            StartCoroutine(cr_letMove());
        }
    }

    private IEnumerator cr_letMove() {
        yield return new WaitForSeconds(0.25f);
        lastWrongDirection = direction.NONE;
    }

    void Update () {
        // Inputs
        // Restart / Time Speed
        if (canSpeedUpTime) {
            if (Input.GetButtonDown("Jump")) {
                Time.timeScale = 2;
                if (music != null)
                    music.GetComponent<AudioSource>().pitch = 2;

                canMove = false;

                if (speedUpIcon != null)
                    speedUpIcon.StartAnimation();
            }

            if (Input.GetButtonUp("Jump")) {
                Time.timeScale = 1;

                if(music != null)
                    music.GetComponent<AudioSource>().pitch = 1;

                canMove = true;

                if(speedUpIcon != null)
                    speedUpIcon.StopAnimation();
            }
        } else {
            if (Input.GetButtonDown("Jump") && !blackOnStart.firstShow) {
                blackOnStart.startHideScreen();

                blackOnStart.back.color = new Color(blackOnStart.back.color.r,
                                                    blackOnStart.back.color.g,
                                                    blackOnStart.back.color.b,
                                                    blackOnStart.back.color.a + 0.005f);
            }

            if (Input.GetButtonUp("Jump") && !blackOnStart.firstShow) {
                blackOnStart.showScreenBack();
            }

            if (Input.GetButton("Jump") && !blackOnStart.firstShow) {
                blackOnStart.back.color = new Color(blackOnStart.back.color.r,
                                                    blackOnStart.back.color.g,
                                                    blackOnStart.back.color.b,
                                                    blackOnStart.back.color.a + 0.005f);

                if (blackOnStart.back.color.a >= 1) {
                    blackOnStart.Resrtart_scene();
                }
            }
        }

        // Movement
        if (currentDirection == direction.NONE && !movingBack && !isMoving && canMove) {
            float horAxis = Input.GetAxis("Horizontal");
            float verAxis = Input.GetAxis("Vertical");

            if (horAxis <= -0.1f && lastWrongDirection != direction.LEFT) {
                currentDirection = direction.LEFT;
                lastWrongDirection = direction.NONE;
            }

            if (horAxis >= 0.1f && lastWrongDirection != direction.RIGHT) {
                currentDirection = direction.RIGHT;
                lastWrongDirection = direction.NONE;
            }

            if (verAxis <= -0.1f && lastWrongDirection != direction.DOWN) {
                currentDirection = direction.DOWN;
                lastWrongDirection = direction.NONE;
            }

            if (verAxis >= 0.1f && lastWrongDirection != direction.UP) {
                currentDirection = direction.UP;
                lastWrongDirection = direction.NONE;
            }
        } else if(canMove && (movingBack || isMoving)) {
            distCovered = (Time.time - startMovingTime) * moveSpeed;
            moveValue = distCovered / journeyLength;

            transform.position = Vector3.Lerp(startPoint, endPoint, moveValue);
        }

        // Change size
        if (sizeValue < 1) {
            sizeValue += Time.deltaTime * growSpeed;

            changeColorCounter += Time.deltaTime;

            if (changeColorCounter >= 0.1f) {
                spriteRenderer.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                changeColorCounter = 0;
            }

            transform.localScale = Vector3.Lerp(startSize, finishSize, sizeValue);
        }

        if (sizeValue >= 1) {
            spriteRenderer.color = Color.white;
            changeColorCounter = 0;
        }

        // Check walls
        Collider2D[] topColliders = Physics2D.OverlapCircleAll(topOverlapPoint.transform.position, 0.01f, 1 << LayerMask.NameToLayer("Walls"));
        Collider2D[] botomColliders = Physics2D.OverlapCircleAll(bottomOverlapPoint.transform.position, 0.01f, 1 << LayerMask.NameToLayer("Walls"));
        Collider2D[] leftColliders = Physics2D.OverlapCircleAll(leftOverlapPoint.transform.position, 0.01f, 1 << LayerMask.NameToLayer("Walls"));
        Collider2D[] rightColliders = Physics2D.OverlapCircleAll(rightOverlapPoint.transform.position, 0.01f, 1 << LayerMask.NameToLayer("Walls"));

        Collider2D[] topLeftColliders = Physics2D.OverlapCircleAll(topLeftCorner.transform.position, 0.01f, 1 << LayerMask.NameToLayer("Walls"));
        Collider2D[] topRightColliders = Physics2D.OverlapCircleAll(topRightCorner.transform.position, 0.01f, 1 << LayerMask.NameToLayer("Walls"));
        Collider2D[] bottomLeftColliders = Physics2D.OverlapCircleAll(bottomLeftCorner.transform.position, 0.01f, 1 << LayerMask.NameToLayer("Walls"));
        Collider2D[] bottomRightColliders = Physics2D.OverlapCircleAll(bottomRightCorner.transform.position, 0.01f, 1 << LayerMask.NameToLayer("Walls"));

        if (topColliders.Length > 0 && currentDirection == direction.UP ||
            botomColliders.Length > 0 && currentDirection == direction.DOWN ||
            leftColliders.Length > 0 && currentDirection == direction.LEFT ||
            rightColliders.Length > 0 && currentDirection == direction.RIGHT ||
            topLeftColliders.Length > 0 && (currentDirection == direction.UP || currentDirection == direction.LEFT) ||
            topRightColliders.Length > 0 && (currentDirection == direction.UP || currentDirection == direction.RIGHT) ||
            bottomLeftColliders.Length > 0 && (currentDirection == direction.DOWN || currentDirection == direction.LEFT) ||
            bottomRightColliders.Length > 0 && (currentDirection == direction.DOWN || currentDirection == direction.RIGHT)) {
            UndoMovement();
        }
    }

    [ContextMenu("Grow up")]
    public void GrowUp() {
        startGrowingTime = Time.time;
        startSize = transform.localScale;
        finishSize = transform.localScale + new Vector3(GROW_SIZE, GROW_SIZE, 0);
        lastWrongDirection = direction.NONE;

        audioSource.PlayOneShot(growUp);
        //audioSource.PlayOneShot(pickupCoin);

        sizeValue = 0;
    }

    public void StartTeleportation(Vector3 point) {
        if (canMove) {
            StartCoroutine(cr_start_teleportation(point));
            audioSource.PlayOneShot(growUp);
        }
    }

    private Vector3 startScale;

    private IEnumerator cr_start_teleportation(Vector3 point) {
        startScale = transform.localScale;
        currentDirection = direction.NONE;
        lastWrongDirection = direction.NONE;
        canMove = false;

        while (transform.localScale.x > 0.1f) {
            yield return new WaitForSeconds(0.05f);
            transform.localScale = new Vector3(transform.localScale.x - 0.1f, transform.localScale.y - 0.1f, transform.localScale.z);
        }

        transform.position = point;
        startPoint = point;
        endPoint = point;
        StartCoroutine(cr_finish_teleportation());
    }

    private IEnumerator cr_finish_teleportation() {
        while (transform.localScale.x < startScale.x) {
            yield return new WaitForSeconds(0.05f);
            transform.localScale = new Vector3(transform.localScale.x + 0.1f, transform.localScale.y + 0.1f, transform.localScale.z);
        }

        transform.localScale = startScale;
        startScale = Vector3.zero;

        canMove = true;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.green;

        // Sides
        Gizmos.DrawWireSphere(topOverlapPoint.transform.position, 0.01f);
        Gizmos.DrawWireSphere(bottomOverlapPoint.transform.position, 0.01f);
        Gizmos.DrawWireSphere(leftOverlapPoint.transform.position, 0.01f);
        Gizmos.DrawWireSphere(rightOverlapPoint.transform.position, 0.01f);

        // Corners
        Gizmos.DrawWireSphere(topLeftCorner.transform.position, 0.01f);
        Gizmos.DrawWireSphere(topRightCorner.transform.position, 0.01f);
        Gizmos.DrawWireSphere(bottomLeftCorner.transform.position, 0.01f);
        Gizmos.DrawWireSphere(bottomRightCorner.transform.position, 0.01f);
    }
}

public enum direction {
    NONE,
    LEFT,
    RIGHT,
    UP,
    DOWN
}