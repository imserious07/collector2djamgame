﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Coin : MonoBehaviour {
    private Animator anim;
    private bool isTaken = false;

    private void Awake() {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Player player = collision.GetComponent<Player>();

        if (player != null && !isTaken) {
            FindObjectOfType<Door>().pickUpCoin();
            anim.SetBool("isTaken", true);
            isTaken = true;

            player.GrowUp();
        }
    }
}