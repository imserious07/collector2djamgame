﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyMusic : MonoBehaviour {
    public static DontDestroyMusic instnace;

    void Awake () {
        if (instnace == null) {
            instnace = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
	}
}
