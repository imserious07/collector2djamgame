﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseGame : MonoBehaviour {
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Cancel")) {
            print("Closing the game");
            Application.Quit();
        }
	}
}
