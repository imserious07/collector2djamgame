﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Door : MonoBehaviour {
    private Animator anim;
    private Collider2D col;

    [SerializeField]
    public int _coinsToUnlock = 5;

    [SerializeField]
    public int coinsToUnlock {
        set {
            _coinsToUnlock = value;

            if (_coinsToUnlock == 0) {
                UnlockDoor();
            }
        }

        get {
            return _coinsToUnlock;
        }
    }

	// Use this for initialization
	void Awake () {
        anim = GetComponent<Animator>();
        col = GetComponent<Collider2D>();
    }

    public void pickUpCoin() {
        coinsToUnlock--;

        FindObjectOfType<CoinIndicator>().UpdateValue();
    }

    private void UnlockDoor() {
        col.enabled = false;

        anim.SetBool("isOpened", true);
    }
}
