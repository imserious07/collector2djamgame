﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Button : MonoBehaviour {
    public List<WallCommands> wallsTriggeredByThisButton;
    private bool isActivated = false;

    private Animator anim;
    private TipScreen tipScreen;

    private void Awake() {
        anim = GetComponent<Animator>();
        tipScreen = FindObjectOfType<TipScreen>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Player player = collision.GetComponent<Player>();

        if (!isActivated && player != null) {
            anim.SetBool("isActivated", true);
            isActivated = true;

            if (wallsTriggeredByThisButton != null &&
               wallsTriggeredByThisButton.Count > 0) {
                foreach (WallCommands cmd in wallsTriggeredByThisButton) {
                    cmd.wallToApply.ActivateWall(cmd.wallStates);
                }
            }
        }

        if (player != null) {
            player.canSpeedUpTime = true;

            if (tipScreen != null)
                tipScreen.SetTipText("To speed up the time, hold the restart button.");
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        Player player = collision.GetComponent<Player>();

        if (player != null) {
            player.canSpeedUpTime = false;

            if (tipScreen != null)
                tipScreen.SetTipText("");
        }
    }
}