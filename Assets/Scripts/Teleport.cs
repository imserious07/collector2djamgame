﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Teleport : MonoBehaviour {
    public Transform teleportationPoint;

    private void OnTriggerEnter2D(Collider2D collision) {
        Player player = collision.GetComponent<Player>();

        if (player != null && player.canMove) {
            print("teleportation!");
            player.StartTeleportation(new Vector3(teleportationPoint.position.x,
                                                    teleportationPoint.position.y,
                                                    player.gameObject.transform.position.z));
        }
    }
}