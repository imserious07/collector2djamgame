﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideTextOnStart : MonoBehaviour {

    public Text levelTitle;
    public Outline levelTitleOutline;
    public Text discriptionText;
    public Outline discriptionTextOutline;

	void Awake () {
        levelTitle.gameObject.SetActive(true);
        discriptionText.gameObject.SetActive(true);

        StartCoroutine(cr_hide_text());
	}

    private IEnumerator cr_hide_text() {
        yield return new WaitForSeconds(3f);
        while (levelTitle.color.a > 0) {
            yield return new WaitForSeconds(0.05f);
            levelTitle.color = new Color(levelTitle.color.r,
                                         levelTitle.color.g,
                                         levelTitle.color.b,
                                         levelTitle.color.a - 0.01f);

            discriptionText.color = new Color(discriptionText.color.r,
                                              discriptionText.color.g,
                                              discriptionText.color.b,
                                              discriptionText.color.a - 0.01f);

            levelTitleOutline.effectColor = new Color(levelTitleOutline.effectColor.r,
                                                      levelTitleOutline.effectColor.g,
                                                      levelTitleOutline.effectColor.b,
                                                      levelTitleOutline.effectColor.a - 0.01f);

            discriptionTextOutline.effectColor = new Color(discriptionTextOutline.effectColor.r,
                                                      discriptionTextOutline.effectColor.g,
                                                      discriptionTextOutline.effectColor.b,
                                                      discriptionTextOutline.effectColor.a - 0.01f);
        }
    }
}
