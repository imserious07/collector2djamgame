﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipScreen : MonoBehaviour {

    public Text tipText;

    private void Awake() {
        SetTipText("");
    }

    public void SetTipText(string text) {
        if (tipText != null)
            tipText.text = text;
        else
            Debug.LogError("tipText not set!");
    }
}
