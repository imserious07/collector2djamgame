﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedUpIcon : MonoBehaviour {
    public Image icon;

    private bool isPlay = true;
    private IEnumerator cr_animationPlayer;

    private void Awake() {
        icon.color = new Color(icon.color.r, icon.color.g, icon.color.b, 0);
    }

    public void StartAnimation() {
        StopAnimation();

        isPlay = true;
        cr_animationPlayer = cr_playAnimation();
        StartCoroutine(cr_animationPlayer);
    }

    public void StopAnimation() {
        isPlay = false;
        icon.color = new Color(icon.color.r, icon.color.g, icon.color.b, 0);
    }

    private IEnumerator cr_playAnimation() {
        while (isPlay) {
            icon.color = new Color(icon.color.r, icon.color.g, icon.color.b, 1);
            yield return new WaitForSeconds(1f);
            icon.color = new Color(icon.color.r, icon.color.g, icon.color.b, 0);
            yield return new WaitForSeconds(1f);
        }
    }
}