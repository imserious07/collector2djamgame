﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BlackOnStart : MonoBehaviour {
    public Image back;
    public bool firstShow = true;

    private IEnumerator cr_show_screen;

    // Use this for initialization
    void Awake() {
        back.color = new Color(back.color.r, back.color.g, back.color.b, 1);
        StartCoroutine(cr_ShowBlack());
    }

    private IEnumerator cr_ShowBlack() {
        while (back.color.a > 0) {
            yield return new WaitForSeconds(0.05f);

            back.color = new Color(back.color.r, back.color.g, back.color.b, back.color.a - 0.01f);
        }

        firstShow = false;
    }

    public void showScreenBack() {
        if (!firstShow) {
            cr_show_screen = cr_ShowBlack();
            StartCoroutine(cr_show_screen);
        }
    }

    public void startHideScreen() {
        if (!firstShow) {
            if (cr_show_screen != null)
                StopCoroutine(cr_show_screen);
        }
    }

    public void Resrtart_scene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
