﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinIndicator : MonoBehaviour {
    private Text textField;
    private int startCoinAmount;
    private Door door;

    private void Awake() {
        textField = GetComponent<Text>();

        door = FindObjectOfType<Door>();

        startCoinAmount = door.coinsToUnlock;

        textField.text = startCoinAmount - door.coinsToUnlock + " / " + startCoinAmount;
    }

    public void UpdateValue() {
        textField.text = startCoinAmount - door.coinsToUnlock + " / " + startCoinAmount;
    }
}