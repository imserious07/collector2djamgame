﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour {
    public Image backGround;

    public string nextScene;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<Player>()) {
            if (backGround != null) {
                StartCoroutine(cr_ShowBlack());
            }
        }
    }

    private IEnumerator cr_ShowBlack() {
        while (backGround.color.a < 1) {
            yield return new WaitForSeconds(0.05f);

            backGround.color = new Color(backGround.color.r, backGround.color.g, backGround.color.b, backGround.color.a + 0.03f);
        }

        SceneManager.LoadScene(nextScene);
    }
}