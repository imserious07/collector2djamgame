﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinamicWall : MonoBehaviour {
    [Header("Start with state")]
    public BlockWallState startWallState;
    public Sprite WallSprite;
    public Sprite EmptyWallSprite;

    private Animator blockAnimator;
    private SpriteRenderer spriteRenderer;
    private BlockWallState currentWallState;
    public bool wallUpdatingInProgress = false;
    private List<BlockWallState> animationsQueue;

    private List<BlockWallState> animationsQueueCopy;

    public List<WallCommands> wallsTriggeredByThisWall;

    [Header("Debug")]
    public List<BlockWallState> goToState;

    [Header("Animations")]
    public RuntimeAnimatorController topToDown_1;
    public RuntimeAnimatorController topToDown_2;
    public RuntimeAnimatorController downToTop_1;
    public RuntimeAnimatorController downToTop_2;
    public RuntimeAnimatorController leftToRight_1;
    public RuntimeAnimatorController leftToRight_2;
    public RuntimeAnimatorController rightToLeft_1;
    public RuntimeAnimatorController rightToLeft_2;
    public RuntimeAnimatorController leftTopCornerToRightBottomCorner_1;
    public RuntimeAnimatorController leftTopCornerToRightBottomCorner_2;
    public RuntimeAnimatorController rightTopCornerToLeftBottomCorner_1;
    public RuntimeAnimatorController rightTopCornerToLeftBottomCorner_2;
    public RuntimeAnimatorController rightBotomCornerToLeftTopCorner_1;
    public RuntimeAnimatorController rightBotomCornerToLeftTopCorner_2;
    public RuntimeAnimatorController leftBottomCornerToRightTopCorner_1;
    public RuntimeAnimatorController leftBottomCornerToRightTopCorner_2;
    public RuntimeAnimatorController leftTopCornerToRightBottomCorner_in_1;
    public RuntimeAnimatorController leftTopCornerToRightBottomCorner_in_2;
    public RuntimeAnimatorController rightTopCornerToLeftBottomCorner_in_1;
    public RuntimeAnimatorController rightTopCornerToLeftBottomCorner_in_2;
    public RuntimeAnimatorController rightBotomCornerToLeftTopCorner_in_1;
    public RuntimeAnimatorController rightBotomCornerToLeftTopCorner_in_2;
    public RuntimeAnimatorController leftBottomCornerToRightTopCorner_in_1;
    public RuntimeAnimatorController leftBottomCornerToRightTopCorner_in_2;

    [Header("Colliders")]
    public GameObject fullWallCollider;
    public GameObject wall_1;
    public GameObject wall_2;
    public GameObject wall_3;
    public GameObject wall_4;
    public GameObject wall_corner_1;
    public GameObject wall_corner_2;
    public GameObject wall_corner_3;
    public GameObject wall_corner_4;
    public GameObject wall_corner_in_1;
    public GameObject wall_corner_in_2;
    public GameObject wall_corner_in_3;
    public GameObject wall_corner_in_4;

    private bool wallIsDone = false;

    private void Awake() {
        blockAnimator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        setStartState(startWallState);
        currentWallState = startWallState;
        ActivateColliders();
    }

    [ContextMenu("Debug WallState")]
    public void DebugWallAnimation() {
        if (goToState != null && goToState.Count > 0) {
            ActivateWall(goToState);
        }
    }

    private void setStartState(BlockWallState startWallState) {
        switch (startWallState) {
            case BlockWallState.EMPTY:
                spriteRenderer.sprite = EmptyWallSprite;
                blockAnimator.runtimeAnimatorController = null;
                blockAnimator.enabled = true;
                break;
            case BlockWallState.FULL:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = null;
                break;
            case BlockWallState.TOP_TO_DOWN:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = topToDown_2;
                break;
            case BlockWallState.DOWN_TO_TOP:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = downToTop_2;
                break;
            case BlockWallState.LEFT_TO_RIGHT:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = leftToRight_2;
                break;
            case BlockWallState.RIGHT_TO_LEFT:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = rightToLeft_2;
                break;
            case BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = leftTopCornerToRightBottomCorner_2;
                break;
            case BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = rightTopCornerToLeftBottomCorner_2;
                break;
            case BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = rightBotomCornerToLeftTopCorner_2;
                break;
            case BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = leftBottomCornerToRightTopCorner_2;
                break;
            case BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C_IN:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = leftTopCornerToRightBottomCorner_in_2;
                break;
            case BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C_IN:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = rightTopCornerToLeftBottomCorner_in_2;
                break;
            case BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C_IN:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = rightBotomCornerToLeftTopCorner_in_2;
                break;
            case BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C_IN:
                spriteRenderer.sprite = WallSprite;
                blockAnimator.runtimeAnimatorController = leftBottomCornerToRightTopCorner_in_2;
                break;
        }
    }

    public void ActivateWall(List<BlockWallState> states) {
        if (animationsQueue == null || animationsQueue.Count == 0) {
            animationsQueue = new List<BlockWallState>(states.ToArray());
            animationsQueueCopy = new List<BlockWallState>(animationsQueue.ToArray());

            // Если текущая стадия анимации совпадает со стадией в которую нужно перейти
            if (animationsQueue[0].Equals(currentWallState)) {
                ActivateNextStep();
            } else {
                AnimationStep();
            }
        }
    }

    public void ActivateNextStep() {
        wallUpdatingInProgress = false;

        if (animationsQueue.Count > 0 && !wallIsDone) {
            currentWallState = animationsQueue[0];
            ActivateColliders();
            animationsQueue.RemoveAt(0);
            AnimationStep();

            //if (currentWallState == BlockWallState.EMPTY) {
            //    spriteRenderer.sprite = EmptyWallSprite;
            //    blockAnimator.runtimeAnimatorController = null;
            //}

            if (animationsQueue.Count == 0) {
                wallIsDone = true;

                if (wallsTriggeredByThisWall != null && wallsTriggeredByThisWall.Count > 0) {
                    foreach (WallCommands cmd in wallsTriggeredByThisWall) {
                        if (cmd != null && cmd.wallToApply != null && cmd.wallStates != null && cmd.wallStates.Count > 0) {
                            cmd.wallToApply.ActivateWall(cmd.wallStates);
                        }
                    }
                } else {
                    List<DinamicWall> walls = new List<DinamicWall>();

                    for (int i = 0; i < transform.childCount; i++) {
                        DinamicWall wall = transform.GetChild(i).GetComponent<DinamicWall>();

                        if (wall != null) {
                            walls.Add(wall);
                        }
                    }

                    if (walls != null && walls.Count > 0) {
                        foreach (DinamicWall wall in walls) {
                            wall.ActivateWall(animationsQueueCopy);
                        }

                        animationsQueueCopy.Clear();
                    }
                }
            }
        }
    }

    private void ActivateColliders() {
        OffAllColliders();

        if (currentWallState == BlockWallState.EMPTY)
            return;

        switch (currentWallState) {
            case BlockWallState.FULL:
                fullWallCollider.SetActive(true);
                break;
            case BlockWallState.TOP_TO_DOWN:
                wall_3.SetActive(true);
                break;
            case BlockWallState.DOWN_TO_TOP:
                wall_1.SetActive(true);
                break;
            case BlockWallState.LEFT_TO_RIGHT:
                wall_2.SetActive(true);
                break;
            case BlockWallState.RIGHT_TO_LEFT:
                wall_4.SetActive(true);
                break;
            case BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C:
                break;
            case BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C:
                break;
            case BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C:
                break;
            case BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C:
                break;
            case BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C_IN:
                break;
            case BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C_IN:
                break;
            case BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C_IN:
                break;
            case BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C_IN:
                break;
        }
    }

    private void OffAllColliders() {
        fullWallCollider.SetActive(false);
        wall_1.SetActive(false);
        wall_2.SetActive(false);
        wall_3.SetActive(false);
        wall_4.SetActive(false);
        wall_corner_1.SetActive(false);
        wall_corner_2.SetActive(false);
        wall_corner_3.SetActive(false);
        wall_corner_4.SetActive(false);
        wall_corner_in_1.SetActive(false);
        wall_corner_in_2.SetActive(false);
        wall_corner_in_3.SetActive(false);
        wall_corner_in_4.SetActive(false);
    }

    private void OnOffAnimator() {
        blockAnimator.enabled = true;
        blockAnimator.SetBool("isPlayAnimation", true);
        wallUpdatingInProgress = true;
    }

    private void AnimationStep() {
        if (animationsQueue != null && animationsQueue.Count > 0) {
            // Из полного режима блока перевести в недозаполненный
            if (currentWallState.Equals(BlockWallState.FULL) && (
                animationsQueue[0].Equals(BlockWallState.DOWN_TO_TOP) ||
                animationsQueue[0].Equals(BlockWallState.TOP_TO_DOWN) ||
                animationsQueue[0].Equals(BlockWallState.LEFT_TO_RIGHT) ||
                animationsQueue[0].Equals(BlockWallState.RIGHT_TO_LEFT) ||
                animationsQueue[0].Equals(BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C) ||
                animationsQueue[0].Equals(BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C) ||
                animationsQueue[0].Equals(BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C) ||
                animationsQueue[0].Equals(BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C) ||
                animationsQueue[0].Equals(BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C_IN) ||
                animationsQueue[0].Equals(BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C_IN) ||
                animationsQueue[0].Equals(BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C_IN) ||
                animationsQueue[0].Equals(BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C_IN))) {
                switch (animationsQueue[0]) {
                    case BlockWallState.DOWN_TO_TOP:
                        blockAnimator.runtimeAnimatorController = downToTop_1;
                        break;
                    case BlockWallState.TOP_TO_DOWN:
                        blockAnimator.runtimeAnimatorController = topToDown_1;
                        break;
                    case BlockWallState.LEFT_TO_RIGHT:
                        blockAnimator.runtimeAnimatorController = leftToRight_1;
                        break;
                    case BlockWallState.RIGHT_TO_LEFT:
                        blockAnimator.runtimeAnimatorController = rightToLeft_1;
                        break;
                    case BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C:
                        blockAnimator.runtimeAnimatorController = leftTopCornerToRightBottomCorner_1;
                        break;
                    case BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C:
                        blockAnimator.runtimeAnimatorController = rightBotomCornerToLeftTopCorner_1;
                        break;
                    case BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C:
                        blockAnimator.runtimeAnimatorController = rightTopCornerToLeftBottomCorner_1;
                        break;
                    case BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C:
                        blockAnimator.runtimeAnimatorController = leftBottomCornerToRightTopCorner_1;
                        break;
                    case BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C_IN:
                        blockAnimator.runtimeAnimatorController = leftTopCornerToRightBottomCorner_in_1;
                        break;
                    case BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C_IN:
                        blockAnimator.runtimeAnimatorController = rightBotomCornerToLeftTopCorner_in_1;
                        break;
                    case BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C_IN:
                        blockAnimator.runtimeAnimatorController = rightTopCornerToLeftBottomCorner_in_1;
                        break;
                    case BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C_IN:
                        blockAnimator.runtimeAnimatorController = leftBottomCornerToRightTopCorner_in_1;
                        break;
                }

                OnOffAnimator();
            } else if (currentWallState.Equals(BlockWallState.DOWN_TO_TOP) || // Из недозапоненного режима блока перевести в пустой
                currentWallState.Equals(BlockWallState.TOP_TO_DOWN) ||
                currentWallState.Equals(BlockWallState.LEFT_TO_RIGHT) ||
                currentWallState.Equals(BlockWallState.RIGHT_TO_LEFT) ||
                currentWallState.Equals(BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C) ||
                currentWallState.Equals(BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C) ||
                currentWallState.Equals(BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C) ||
                currentWallState.Equals(BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C) ||
                currentWallState.Equals(BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C_IN) ||
                currentWallState.Equals(BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C_IN) ||
                currentWallState.Equals(BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C_IN) ||
                currentWallState.Equals(BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C_IN) &&
                animationsQueue[0].Equals(BlockWallState.EMPTY)) {

                switch (currentWallState) {
                    case BlockWallState.DOWN_TO_TOP:
                        blockAnimator.runtimeAnimatorController = downToTop_2;
                        break;
                    case BlockWallState.TOP_TO_DOWN:
                        blockAnimator.runtimeAnimatorController = topToDown_2;
                        break;
                    case BlockWallState.LEFT_TO_RIGHT:
                        blockAnimator.runtimeAnimatorController = leftToRight_2;
                        break;
                    case BlockWallState.RIGHT_TO_LEFT:
                        blockAnimator.runtimeAnimatorController = rightToLeft_2;
                        break;
                    case BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C:
                        blockAnimator.runtimeAnimatorController = leftTopCornerToRightBottomCorner_2;
                        break;
                    case BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C:
                        blockAnimator.runtimeAnimatorController = rightBotomCornerToLeftTopCorner_2;
                        break;
                    case BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C:
                        blockAnimator.runtimeAnimatorController = rightTopCornerToLeftBottomCorner_2;
                        break;
                    case BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C:
                        blockAnimator.runtimeAnimatorController = leftBottomCornerToRightTopCorner_2;
                        break;
                    case BlockWallState.LEFT_TOP_C_TO_RIGHT_BOT_C_IN:
                        blockAnimator.runtimeAnimatorController = leftTopCornerToRightBottomCorner_in_2;
                        break;
                    case BlockWallState.RIGHT_BOTTOM_C_TO_LEFT_TOP_C_IN:
                        blockAnimator.runtimeAnimatorController = rightBotomCornerToLeftTopCorner_in_2;
                        break;
                    case BlockWallState.RIGHT_TOP_C_TO_LEFT_BOT_C_IN:
                        blockAnimator.runtimeAnimatorController = rightTopCornerToLeftBottomCorner_in_2;
                        break;
                    case BlockWallState.LEFT_BOTTOM_C_TO_RIGHT_TOP_C_IN:
                        blockAnimator.runtimeAnimatorController = leftBottomCornerToRightTopCorner_in_2;
                        break;
                }

                OnOffAnimator();
            }
        }
    }
}

[System.Serializable]
public class WallCommands {
    public DinamicWall wallToApply;
    public List<BlockWallState> wallStates;
}

public enum BlockWallState {
    EMPTY,
    FULL,
    TOP_TO_DOWN,
    DOWN_TO_TOP,
    LEFT_TO_RIGHT,
    RIGHT_TO_LEFT,
    LEFT_TOP_C_TO_RIGHT_BOT_C,
    RIGHT_TOP_C_TO_LEFT_BOT_C,
    RIGHT_BOTTOM_C_TO_LEFT_TOP_C,
    LEFT_BOTTOM_C_TO_RIGHT_TOP_C,
    LEFT_TOP_C_TO_RIGHT_BOT_C_IN,
    RIGHT_TOP_C_TO_LEFT_BOT_C_IN,
    RIGHT_BOTTOM_C_TO_LEFT_TOP_C_IN,
    LEFT_BOTTOM_C_TO_RIGHT_TOP_C_IN,
}